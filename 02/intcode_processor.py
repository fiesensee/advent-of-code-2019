def read_intcode_programm():
    with open("02/intcodes.txt") as f:
        intcode_text = f.readline()
    string_intcodes = intcode_text.split(",")
    intcodes = []
    for intcode in string_intcodes:
        intcodes.append(int(intcode))
    return intcodes

def process_intcodes(intcodes):
    pointer = 0
    while True:
        intcode = intcodes[pointer]
        if intcode == 99:
            break
        elif intcode == 1:
            x = intcodes[intcodes[pointer + 1]]
            y = intcodes[intcodes[pointer + 2]]
            position = intcodes[pointer + 3]
            sum = x + y
            intcodes[position] = sum
            pointer += 4
        elif intcode == 2:
            x = intcodes[intcodes[pointer + 1]]
            y = intcodes[intcodes[pointer + 2]]
            position = intcodes[pointer + 3]
            product = x * y
            intcodes[position] = product
            pointer += 4
        else:
            pointer += 1
    return intcodes

def find_values(intcodes):
    result = 0
    for noun in range(100):
        for verb in range(100):
            new_intcodes = intcodes.copy()
            new_intcodes[1] = noun
            new_intcodes[2] = verb
            result = process_intcodes(new_intcodes)
            if result[0] == 19690720:
                return 100 * noun + verb
    print("nothing found")
    return 0

    

if __name__ == "__main__":
    intcodes = read_intcode_programm()
    print(process_intcodes(intcodes.copy()))
    print(find_values(intcodes.copy()))