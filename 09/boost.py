from intcode_processor import IntcodeProcessor

def read_intcode_programm():
    with open("09/input.txt") as f:
        intcode_text = f.readline()
    string_intcodes = intcode_text.split(",")
    intcodes = []
    for intcode in string_intcodes:
        intcodes.append(int(intcode))
    return intcodes

if __name__ == "__main__":
    intcode = read_intcode_programm()
    processor = IntcodeProcessor(intcode)
    processor.process_intcode_with_input(2)
    print(processor.output)
    # print(processor.state)

