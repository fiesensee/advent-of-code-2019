from treelib import Tree, Node

def get_orbits():
    lines = ""
    with open('06/input.txt') as f:
        lines = f.readlines()
    return lines

def build_orbit_tree(orbits):
    orbit_tree = Tree()
    orbit_tree.create_node("COM", "COM")

    for orbit in orbits:
        center, satellite = orbit.strip().split(")")
        get_node(orbit_tree, center)
        get_node(orbit_tree, satellite)
        orbit_tree.move_node(satellite, center)

    return orbit_tree

def get_node(orbit_tree, identifier):
    if identifier in orbit_tree:
        return orbit_tree.get_node(identifier)
    else:
        return orbit_tree.create_node(identifier, identifier, parent="COM")

def count_orbits(orbit_tree):
    orbits = 0
    for node in orbit_tree.all_nodes():
        orbits += orbit_tree.depth(node)
    return orbits

def count_transfer(orbit_tree, source, destination):
    common_node = find_common_node(orbit_tree, source, destination)
    common_node_depth = orbit_tree.depth(common_node)
    source_depth = orbit_tree.depth(source)
    destination_depth = orbit_tree.depth(destination)
    return (source_depth - common_node_depth) + (destination_depth - common_node_depth) - 2

def find_common_node(orbit_tree, node1, node2):
    tree_node1 = orbit_tree.get_node(node1)
    depth = orbit_tree.depth(tree_node1)
    while depth > 1:
        if get_ancestor_at_level(orbit_tree, node1, depth) == get_ancestor_at_level(orbit_tree, node2, depth):
            return get_ancestor_at_level(orbit_tree, node1, depth)
        depth -= 1

def get_ancestor_at_level(orbit_tree, node, level):
    parent_node = orbit_tree.parent(node)
    depth = orbit_tree.depth(parent_node)
    while depth > level:
        parent_node = orbit_tree.parent(parent_node.identifier)
        depth -= 1
    return parent_node.identifier

if __name__ == "__main__":
    orbits = get_orbits()

    orbit_tree = build_orbit_tree(orbits)
    # orbit_tree.show()

    print(count_orbits(orbit_tree))
    print(get_ancestor_at_level(orbit_tree, "YOU", 0))
    print(find_common_node(orbit_tree, "YOU", "SAN"))
    print(count_transfer(orbit_tree, "YOU", "SAN"))