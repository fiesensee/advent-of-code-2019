import math

class FuelMixer:

    def __init__(self):
        self.reactions = []
        self.storage = []

    def read_reactions(self):

        with open("14/input.txt") as f:
            for reaction_line in f.readlines():           
                reaction = {}
                inputs, output = reaction_line.strip().split(" => ")
                output_amount, output_compound = output.split(" ")
                reaction["output_amount"] = int(output_amount)
                reaction["output_compound"] = output_compound
                input_values = []
                for input in inputs.split(","):
                    input_amount, input_compound = input.strip().split(" ")
                    input_value = {"input_amount": int(input_amount), "input_compound": input_compound}
                    input_values.append(input_value)
                reaction["inputs"] = input_values
                self.reactions.append(reaction)

    def find_reaction_by_output(self, compound):
        for reaction in self.reactions:
            if reaction["output_compound"] == compound:
                return reaction

    def is_stored(self, compound, amount):
        # print("checking {} for {} {}".format(self.storage, compound, amount))
        for stored_compound in self.storage:
            if stored_compound["compound"] == compound and stored_compound["amount"] >= amount:
                return True
        return False

    def remove_from_storage(self, compound, amount):
        for stored_compound in self.storage:
            if stored_compound["compound"] == compound:
                if stored_compound["amount"] >= amount:
                    stored_compound["amount"] -= amount
                    return amount
                else:
                    stored_amount = stored_compound["amount"]
                    stored_compound["amount"] = 0
                    return stored_amount
        print("Error: Took out while there was none")

    def add_to_storage(self, compound, amount):
        if amount == 0:
            return
        # print("adding to storage: {} {}".format(compound, amount))
        for stored_compound in self.storage:
            if stored_compound["compound"] == compound:
                stored_compound["amount"] += amount
                return
        new_stored_compound = {"compound": compound, "amount": amount}
        self.storage.append(new_stored_compound)

    def calculate_compound(self, base, compound, multiplicator):
        ore_counter = 0
        reaction = self.find_reaction_by_output(compound)
        # print("Producing {} * {} {}".format(multiplicator, reaction["output_amount"], compound))
        for input in reaction["inputs"]:
            if input["input_compound"] == base:
                # print("adding {} ore".format(multiplicator * input["input_amount"]))
                ore_counter += multiplicator * input["input_amount"]
            else:
                needed_input = input["input_amount"] * multiplicator
                # check if required amount is in storage
                if self.is_stored(input["input_compound"], 0):
                    # print("getting from storage")
                    stored_compound = self.remove_from_storage(input["input_compound"], needed_input)
                    needed_input -= stored_compound
                    if needed_input == 0:
                        continue
                follow_reaction = self.find_reaction_by_output(input["input_compound"])
                if follow_reaction["output_amount"] > needed_input:
                    # add rest to the storage
                    new_multiplicator = 1
                    ore_counter += self.calculate_compound(base, follow_reaction["output_compound"], new_multiplicator)
                    self.add_to_storage(follow_reaction["output_compound"], follow_reaction["output_amount"] - needed_input)
                elif follow_reaction["output_amount"] == needed_input:
                    # new extra multiplicator is 1
                    new_multiplicator = 1
                    ore_counter += self.calculate_compound(base, follow_reaction["output_compound"], new_multiplicator)
                else:
                    # new extra multiplicator needs to be ceiling(input / output)
                    new_multiplicator = math.ceil(needed_input / follow_reaction["output_amount"])
                    ore_counter += self.calculate_compound(base, follow_reaction["output_compound"], new_multiplicator)
                    self.add_to_storage(follow_reaction["output_compound"], (follow_reaction["output_amount"] * new_multiplicator) - needed_input)
        return ore_counter

    def calculate_minimum_ore(self):
        self.read_reactions()
        minimum_ore = self.calculate_compound("ORE", "FUEL", 1)
        print(self.storage)
        print(minimum_ore)
    
    def calculate_max_fuel(self, ore_amount):
        self.read_reactions()
        minimum_ore = 0
        # fuel_amount = math.floor(1000000000000 / 114125)
        fuel_amount = 12039300
        while minimum_ore <= ore_amount:
            self.storage = []
            minimum_ore = self.calculate_compound("ORE", "FUEL", fuel_amount)
            fuel_amount += 1
        print(fuel_amount - 2)


if __name__ == "__main__":
    fuel_mixer = FuelMixer()
    # fuel_mixer.calculate_minimum_ore()
    fuel_mixer.calculate_max_fuel(1000000000000)
