import math

def read_wires():
    with open("03/input.txt") as f:
        wire1 = f.readline().split(",")
        wire2 = f.readline().split(",")
    return [wire1, wire2]

def draw_wire(board, wire, wire_symbol):
    middle = math.floor(len(board) / 2)
    current_position = [middle, middle]
    board[current_position[0]][current_position[1]] = 0
    crossings = []
    for point in wire:
        current_position = calculate_next_position(board, crossings, current_position, point, wire_symbol)
    return crossings

def calculate_next_position(board, crossings, current_position, vector, wire_symbol):
    direction = vector[0]
    distance = int(vector[1:])
    if direction == "R":
        for _ in range(distance):
            current_position = [current_position[0] + 1, current_position[1]]
            draw_line(board, current_position, crossings, wire_symbol)
    elif direction == "L":
        for _ in range(distance):
            current_position = [current_position[0] - 1, current_position[1]]
            draw_line(board, current_position, crossings, wire_symbol)
    elif direction == "U":
        for _ in range(distance):
            current_position = [current_position[0], current_position[1] + 1]
            draw_line(board, current_position, crossings, wire_symbol)
    elif direction == "D":
        for _ in range(distance):
            current_position = [current_position[0], current_position[1] - 1]
            draw_line(board, current_position, crossings, wire_symbol)
    return current_position

def draw_line(board, current_position, crossings, wire_symbol):
    # while len(board) <= current_position[1]:
        # board.append([])
    column = board[current_position[1]]
    # while len(column) <= current_position[0]:
        # column.append(".")
    if column[current_position[0]] != wire_symbol and column[current_position[0]] != ".":
        column[current_position[0]] = "x"
        crossings.append([current_position[0], current_position[1]])
    else:
        column[current_position[0]] = wire_symbol

def find_closest_crossing(board, crossings, start_point):
    smallest_distance = 10000000
    nearest_crossing = []
    for crossing in crossings:
        distance = abs(start_point[0] - crossing[0]) + abs(start_point[1] - crossing[1])
        if distance < smallest_distance:
            smallest_distance = distance
            nearest_crossing = crossing
    return smallest_distance, nearest_crossing

def find_closest_crossing_by_length(board, crossings, wires, start_position):
    smallest_distance = 1000000
    nearest_crossing = []
    original_start = start_position.copy()
    print("crossings: {}".format(len(crossings)))
    for crossing in crossings:
        distance1 = get_distance_to_point(crossing, wires[0], original_start.copy())
        distance2 = get_distance_to_point(crossing, wires[1], original_start.copy())
        total = distance1 + distance2
        if total < smallest_distance:
            smallest_distance = total
            nearest_crossing = crossing
    return smallest_distance, nearest_crossing

def get_distance_to_point(crossing, wire, start_position):
    distance = 0
    for line in wire:
        target_distance = int(line[1:])
        walked_distance = walk_till_crosses(line, crossing, start_position)
        # print(start_position)
        distance += walked_distance
        if walked_distance != target_distance:
            break

    return distance

def walk_till_crosses(line, crossing, position):
    direction = line[0]
    distance = int(line[1:])
    walked_distance = 0
    while walked_distance < distance:
        if position == crossing:
            break
        if direction == "R":
            position[0] = position[0] + 1
        elif direction == "L":
            position[0] = position[0] - 1
        elif direction == "U":
            position[1] = position[1] + 1
        elif direction == "D":
            position[1] = position[1] - 1
        walked_distance += 1
    return walked_distance

if __name__ == "__main__":
    wires = read_wires()
    board = []
    size = 25000
    for _ in range(size):
        column = []
        for _ in range(size):
            column.append(".")
        board.append(column)
    middle = math.floor(len(board) / 2)
    start_position = [middle, middle]
    board[middle][middle] = "o"
    print("finished setting up board")
    part_1_board = board.copy()
    draw_wire(part_1_board, wires[0], "-")
    print("finished drawing wire 1")
    crossings = draw_wire(part_1_board, wires[1], "|")
    print("finished drawing wire 2")
    print(part_1_board[middle][middle])
    print(find_closest_crossing(part_1_board, crossings, start_position))

    part_2_board = board.copy()

    print(find_closest_crossing_by_length(part_2_board, crossings, wires, start_position))



