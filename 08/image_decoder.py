import sys

def read_image_data():
    data = ""
    with open("08/input.txt") as f:
        data = f.readline()
    return data

def create_layers(image_data):
    layers = []

    width_index = 0
    height_index = 0
    current_layer = create_empty_layer()

    for i in range(len(image_data)):
        current_layer[width_index][height_index] = image_data[i]
        if width_index == 24:
            width_index = 0
            if height_index == 5:
                height_index = 0
                layers.append(current_layer)
                current_layer = create_empty_layer()
            else:
                height_index += 1
        else:
            width_index += 1

    return layers

def create_empty_layer():
    layer = []
    for _ in range(25):
        row = []
        layer.append(row)
        for _ in range(6):
            row.append(0)
    
    return layer

def find_part1_layer(layers):
    least_zero_layer = []
    least_zeros = 10000000000

    for layer in layers:
        current_layer_zeroes = 0
        for row in layer:
            current_layer_zeroes += row.count('0')
        if current_layer_zeroes < least_zeros:
            least_zeros = current_layer_zeroes
            least_zero_layer = layer

    return least_zero_layer

def calculate_part1_result(layer):
    digit_one_count = 0
    digit_two_count = 0

    for row in layer:
        digit_one_count += row.count('1')
        digit_two_count += row.count('2')

    return digit_one_count * digit_two_count

def decode_image(layers):
    image = create_empty_layer()

    for i in range(len(image)):
        for j in range(len(image[i])):
            for layer in layers:
                pixel = layer[i][j]
                if pixel == '0' or pixel == '1':
                    image[i][j] = pixel
                    break
    
    return image

def draw_image(image):
    image_text = ""
    # for row in image:
    #     for pixel in row:
    for i in range(6):
        for j in range(25):
            pixel = image[j][i]
            if pixel == '2':
                image_text += " "
            elif pixel == '0':
                image_text += "@"
            elif pixel == '1':
                image_text += "O"
        image_text += "\n"

    print(image_text)


if __name__ == "__main__":

    image_data = read_image_data()
    layers = create_layers(image_data)
    print(layers[0])
    layer = find_part1_layer(layers)
    print(layer)
    result = calculate_part1_result(layer)
    print(result)
    image = decode_image(layers)
    draw_image(image)