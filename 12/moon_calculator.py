import itertools
import time
import functools
import math

class MoonCalculator:

    def __init__(self, axes):
        self.axes = axes

    def read_moons(self):
        moons = []

        with open("12/test_input.txt") as f:
            moon_lines = f.readlines()
            for moon_line in moon_lines:
                moon_line = moon_line.replace("<", "").replace(">", "")
                moon_settings = moon_line.split(", ")
                moon = {"position": {}, "velocity": {"x": 0, "y": 0, "z": 0}}
                for moon_setting in moon_settings:
                    values = moon_setting.split("=")
                    moon["position"][values[0]] = int(values[1])
                moons.append(moon)

        return moons

    def apply_gravity(self, moon_combinations):
        for pair in moon_combinations:
            moon1_position = pair[0]["position"]
            moon1_velocity = pair[0]["velocity"]
            moon2_position = pair[1]["position"]
            moon2_velocity = pair[1]["velocity"]
            for axis in self.axes:
                if moon1_position[axis] < moon2_position[axis]:
                    moon1_velocity[axis] += 1
                    moon2_velocity[axis] -= 1
                if moon1_position[axis] > moon2_position[axis]:
                    moon1_velocity[axis] -= 1
                    moon2_velocity[axis] += 1

    
    def apply_velocity(self, moons):
        for moon in moons:
            for axis in self.axes:
                moon["position"][axis] += moon["velocity"][axis]

    def calculate_total_energy(self, moons):
        total_energy = 0
        for moon in moons:
            total_energy += self.calculate_energy(
                moon["position"]) * self.calculate_energy(moon["velocity"])
        return total_energy

    def calculate_energy(self, vector):
        energy = 0
        for axis in self.axes:
            energy += abs(vector[axis])

        return energy

    def run_calculation(self):
        universe_states = {}
        moons = self.read_moons()
        moon_combinations = list(itertools.combinations(moons, 2))
        current_universe_state = str(moons)
        first_state = str(moons)
        first_moon = str(moons[0])
        total_energy = 0
        cycles = 0
        gravity_time = 0
        velocity_time = 0
        checking_time = 0
        while True:
            start = time.perf_counter()
            self.apply_gravity(moon_combinations)
            gravity_time += time.perf_counter() - start

            start = time.perf_counter()
            self.apply_velocity(moons)
            velocity_time += time.perf_counter() - start


            cycles += 1

            start = time.perf_counter()
            current_first_moon = str(moons[0])
            if current_first_moon == first_moon:
                current_universe_state = str(moons)
                if current_universe_state == first_state:
                    break
            checking_time += time.perf_counter() - start
            if cycles % 100000 == 0:
                total_time = gravity_time + velocity_time + checking_time
                print("{}:\nGravity Time: {}\nVelocity Time: {}\nChecking Time:{}\nTotal: {}".format(
                    cycles, gravity_time, velocity_time, checking_time, total_time))
                gravity_time = 0
                velocity_time = 0
                checking_time = 0

        # print(total_energy)
        # print(moons)
        # print(cycles)
        return cycles

def gcd(a,b):
    """Compute the greatest common divisor of a and b"""
    while b > 0:
        a, b = b, a % b
    return a
    
def lcm(a, b):
    """Compute the lowest common multiple of a and b"""
    return a * b / gcd(a, b)

if __name__ == "__main__":
    # axes = ["x", "y", "z"]
    axes = ["x"]
    calc = MoonCalculator(axes)
    x_return = calc.run_calculation()
    print(x_return)
    axes = ["y"]
    calc = MoonCalculator(axes)
    y_return = calc.run_calculation()
    print(y_return)
    axes = ["z"]
    calc = MoonCalculator(axes)
    z_return = calc.run_calculation()
    print(z_return)
    print(lcm(x_return, lcm(y_return, z_return)))