import itertools

def read_intcode_programm():
    with open("07/input.txt") as f:
        intcode_text = f.readline()
    string_intcodes = intcode_text.split(",")
    intcodes = []
    for intcode in string_intcodes:
        intcodes.append(int(intcode))
    return intcodes

def process_intcodes(intcodes, phase_setting, amp_input):
    pointer = 0
    input_is_phase_setting = True
    while True:
        intcode = intcodes[pointer]
        op_code = int(str(intcode)[-2:])
        if intcode == 99:
            break
        # if len(str(intcode)) > 4:
            # pointer += 1
        elif op_code == 1:
            x, y = get_2_parameters(intcodes, intcode, pointer)

            position = intcodes[pointer + 3]
            sum = x + y
            intcodes[position] = sum
            pointer += 4
            # print("Opcode: {} X: {} Y: {} Result: {} Write To: {}".format(intcode, x, y, sum, position))
        elif op_code == 2:
            x, y = get_2_parameters(intcodes, intcode, pointer)

            position = intcodes[pointer + 3]
            product = x * y
            intcodes[position] = product
            pointer += 4
            # print("Opcode: {} X: {} Y: {} Result: {} Write To: {}".format(intcode, x, y, product, position))
        elif intcode == 3:
            if input_is_phase_setting:
                # print("giving input {}".format(phase_setting))
                intcodes[intcodes[pointer + 1]] = phase_setting
                input_is_phase_setting = False
            else:
                # print("giving input {}".format(amp_input))
                intcodes[intcodes[pointer + 1]] = amp_input
                input_is_phase_setting = True
            pointer += 2
        elif intcode == 4:
            # print("Output: {}".format(intcodes[intcodes[pointer + 1]]))
            amp_input = intcodes[intcodes[pointer + 1]]
            pointer += 2
        elif op_code == 5:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            if x != 0:
                pointer = y
            else:
                pointer += 3
        elif op_code == 6:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            if x == 0:
                pointer = y
            else:
                pointer += 3
        elif op_code == 7:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            position = intcodes[pointer + 3]
            if x < y:
                intcodes[position] = 1
            else:
                intcodes[position] = 0
            pointer += 4
        elif op_code == 8:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            position = intcodes[pointer + 3]
            if x == y:
                intcodes[position] = 1
            else:
                intcodes[position] = 0
            pointer += 4
        else:
            pointer += 1
    return amp_input

def process_intcodes2(intcodes, pointer, amp_input):
    last_output = None
    is_consumed = False
    while True:
        intcode = intcodes[pointer]
        op_code = int(str(intcode)[-2:])
        if intcode == 99:
            return pointer, last_output, True
        elif op_code == 1:
            x, y = get_2_parameters(intcodes, intcode, pointer)

            position = intcodes[pointer + 3]
            sum = x + y
            intcodes[position] = sum
            pointer += 4
            # print("Opcode: {} X: {} Y: {} Result: {} Write To: {}".format(intcode, x, y, sum, position))
        elif op_code == 2:
            x, y = get_2_parameters(intcodes, intcode, pointer)

            position = intcodes[pointer + 3]
            product = x * y
            intcodes[position] = product
            pointer += 4
            # print("Opcode: {} X: {} Y: {} Result: {} Write To: {}".format(intcode, x, y, product, position))
        elif intcode == 3:
            if is_consumed:
                # pointer += 2
                return pointer, last_output, False
            intcodes[intcodes[pointer + 1]] = amp_input
            is_consumed = True
            pointer += 2
        elif intcode == 4:
            # print("Output: {}".format(intcodes[intcodes[pointer + 1]]))
            last_output = intcodes[intcodes[pointer + 1]]
            pointer += 2
        elif op_code == 5:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            if x != 0:
                pointer = y
            else:
                pointer += 3
        elif op_code == 6:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            if x == 0:
                pointer = y
            else:
                pointer += 3
        elif op_code == 7:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            position = intcodes[pointer + 3]
            if x < y:
                intcodes[position] = 1
            else:
                intcodes[position] = 0
            pointer += 4
        elif op_code == 8:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            position = intcodes[pointer + 3]
            if x == y:
                intcodes[position] = 1
            else:
                intcodes[position] = 0
            pointer += 4
        else:
            pointer += 1
    # return amp_input

def get_2_parameters(intcodes, current_intcode, pointer):
    x = None
    y = None
    if len(str(current_intcode)) == 1:
        x = intcodes[intcodes[pointer + 1]]
        y = intcodes[intcodes[pointer + 2]]
    elif len(str(current_intcode)) == 3:
        # is 102 in any case
        x = intcodes[pointer + 1]
        y = intcodes[intcodes[pointer + 2]]
    elif len(str(current_intcode)) == 4:
        # can be 1102 or 1002
        if str(current_intcode)[1] == "0":
            # is 1002
            x = intcodes[intcodes[pointer + 1]]
            y = intcodes[pointer + 2]
        else:
            # is 1102
            x = intcodes[pointer + 1]
            y = intcodes[pointer + 2]

    return x, y

def get_result_for_configuration(intcodes, config):
    ampA = intcodes.copy()
    pointerA = 0
    ampB = intcodes.copy()
    pointerB = 0
    ampC = intcodes.copy()
    pointerC = 0
    ampD = intcodes.copy()
    pointerD = 0
    ampE = intcodes.copy()
    pointerE = 0

    # setup amps
    pointerA, _, _ = process_intcodes2(ampA, pointerA, config[0])
    pointerB, _, _ = process_intcodes2(ampB, pointerB, config[1])
    pointerC, _, _ = process_intcodes2(ampC, pointerC, config[2])
    pointerD, _, _ = process_intcodes2(ampD, pointerD, config[3])
    pointerE, _, _ = process_intcodes2(ampE, pointerE, config[4])

    isFinished = False
    output = 0

    while not isFinished:
        pointerA, output, _ = process_intcodes2(ampA, pointerA, output)
        pointerB, output, _ = process_intcodes2(ampB, pointerB, output)
        pointerC, output, _ = process_intcodes2(ampC, pointerC, output)
        pointerD, output, _ = process_intcodes2(ampD, pointerD, output)
        pointerE, output, isFinished = process_intcodes2(ampE, pointerE, output)

    return output

if __name__ == "__main__":
    max_output = 0
    intcodes = read_intcode_programm()
    # phase_settings = itertools.permutations(range(5), 5)
    # for phase_setting in phase_settings:
    #     # print(phase_setting)
    #     result = 0
    #     for amp in phase_setting:
    #         result = process_intcodes(intcodes.copy(), amp, result)
    #         # print("returned {}".format(result))
    #     if result > max_output:
    #         max_output = result

    # print(max_output)



    # test = process_intcodes2(intcodes.copy(), [9,8,7,6,5], 0)

    phase_settings = itertools.permutations(range(5,10), 5)
    for phase_setting in phase_settings:
        print(phase_setting)
        result = get_result_for_configuration(intcodes.copy(), phase_setting)
        print("returned {}".format(result))
        if result > max_output:
            max_output = result

    print(max_output)

    # process_intcodes(intcodes.copy())
    # print(find_values(intcodes.copy()))
