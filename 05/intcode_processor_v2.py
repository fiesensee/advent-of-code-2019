def read_intcode_programm():
    with open("05/input.txt") as f:
        intcode_text = f.readline()
    string_intcodes = intcode_text.split(",")
    intcodes = []
    for intcode in string_intcodes:
        intcodes.append(int(intcode))
    return intcodes

def process_intcodes(intcodes):
    pointer = 0
    while True:
        intcode = intcodes[pointer]
        op_code = int(str(intcode)[-2:])
        if intcode == 99:
            break
        # if len(str(intcode)) > 4:
            # pointer += 1
        elif op_code == 1:
            x, y = get_2_parameters(intcodes, intcode, pointer)

            position = intcodes[pointer + 3]
            sum = x + y
            intcodes[position] = sum
            pointer += 4
            # print("Opcode: {} X: {} Y: {} Result: {} Write To: {}".format(intcode, x, y, sum, position))
        elif op_code == 2:
            x, y = get_2_parameters(intcodes, intcode, pointer)

            position = intcodes[pointer + 3]
            product = x * y
            intcodes[position] = product
            pointer += 4
            # print("Opcode: {} X: {} Y: {} Result: {} Write To: {}".format(intcode, x, y, product, position))
        elif intcode == 3:
            # value = input("Input:")
            print("Input:5")
            value = 5
            intcodes[intcodes[pointer + 1]] = int(value)
            pointer += 2
        elif intcode == 4:
            print("Output: {}".format(intcodes[intcodes[pointer + 1]]))
            pointer += 2
        elif op_code == 5:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            if x != 0:
                pointer = y
            else:
                pointer += 3
        elif op_code == 6:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            if x == 0:
                pointer = y
            else:
                pointer += 3
        elif op_code == 7:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            position = intcodes[pointer + 3]
            if x < y:
                intcodes[position] = 1
            else:
                intcodes[position] = 0
            pointer += 4
        elif op_code == 8:
            x, y = get_2_parameters(intcodes, intcode, pointer)
            position = intcodes[pointer + 3]
            if x == y:
                intcodes[position] = 1
            else:
                intcodes[position] = 0
            pointer += 4
        else:
            pointer += 1
    return intcodes

def get_2_parameters(intcodes, current_intcode, pointer):
    x = None
    y = None
    if len(str(current_intcode)) == 1:
        x = intcodes[intcodes[pointer + 1]]
        y = intcodes[intcodes[pointer + 2]]
    elif len(str(current_intcode)) == 3:
        # is 102 in any case
        x = intcodes[pointer + 1]
        y = intcodes[intcodes[pointer + 2]]
    elif len(str(current_intcode)) == 4:
        # can be 1102 or 1002
        if str(current_intcode)[1] == "0":
            # is 1002
            x = intcodes[intcodes[pointer + 1]]
            y = intcodes[pointer + 2]
        else:
            # is 1102
            x = intcodes[pointer + 1]
            y = intcodes[pointer + 2]

    return x, y

if __name__ == "__main__":
    intcodes = read_intcode_programm()
    process_intcodes(intcodes.copy())
    # print(find_values(intcodes.copy()))
