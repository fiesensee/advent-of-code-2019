import math

def get_space_map():
    space_map = []
    lines = []
    with open("10/input.txt") as f:
        lines = f.readlines()
    for line in lines:
        space_line = []
        for point in line.strip():
            space_line.append(point)
        space_map.append(space_line)
    
    return space_map

def find_best_station(space_map):
    max_asteroids = 0
    best_station = []

    for y in range(len(space_map)):
        for x in range(len(space_map[y])):
            point = space_map[y][x]
            if point == ".":
                continue
            else:
                visible_asteroids = count_visible_asteroids(space_map, x, y)
                if max_asteroids < visible_asteroids:
                    max_asteroids = visible_asteroids
                    best_station = {"x": x, "y": y}

    return max_asteroids, best_station

def count_visible_asteroids(space_map, origin_x, origin_y):
    visible_asteroid_degrees = []

    for y in range(len(space_map)):
        for x in range(len(space_map[y])):
            point = space_map[y][x]
            if x == origin_x and y == origin_y:
                continue
            if point != ".":
                degree = calculate_degree(origin_x, origin_y, x, y)
                if degree not in visible_asteroid_degrees:
                    visible_asteroid_degrees.append(degree)


    return len(visible_asteroid_degrees)

def calculate_degree(origin_x, origin_y, x, y):
    degree = math.degrees(math.atan2((x - origin_x), -(y - origin_y)))
    if degree < 0:
        degree = 360 - abs(degree)
    return degree

def draw_space_map(space_map):
    result = ""
    for line in space_map:
        for point in line:
            result += point
        result += "\n"
    print(result)

def destroy_next_asteroid(space_map, station, current_angle):
    asteroid, new_angle = get_closest_asteroid(space_map, station, current_angle)
    space_map[asteroid["y"]][asteroid["x"]] = "."
    return asteroid, new_angle

def get_closest_asteroid(space_map, station, current_angle):
    smallest_degree = 360
    next_asteroids = []
    for y in range(len(space_map)):
        for x in range(len(space_map[y])):
            point = space_map[y][x]
            if point == "#":
                degree = calculate_degree(station["x"], station["y"], x, y)
                if degree > current_angle and degree <= smallest_degree:
                    smallest_degree = degree
                    next_asteroids = [asteroid for asteroid in next_asteroids if asteroid["angle"] == degree]
                    next_asteroids.append({"x": x, "y": y, "angle": degree})
    
    smallest_distance = 1000000
    nearest_asteroid = None
    for asteroid in next_asteroids:
        distance = math.sqrt(math.pow(asteroid["x"] - station["x"], 2) + math.pow(asteroid["y"] - station["y"], 2))
        if distance < smallest_distance:
            smallest_distance = distance
            nearest_asteroid = asteroid

    if nearest_asteroid == None:
        return get_closest_asteroid(space_map, station, -1)
    
    return nearest_asteroid, smallest_degree


if __name__ == "__main__":
    space_map = get_space_map()
    most_asteroids, best_station = find_best_station(space_map)
    print(most_asteroids, best_station)
    space_map[best_station["y"]][best_station["x"]] = "O"
    current_angle = -1
    for i in range(200):
        destroyed_asteroid, current_angle = destroy_next_asteroid(space_map, best_station, current_angle)
        print("{}. :".format(i + 1), destroyed_asteroid)
        if i == 199:
            print(destroyed_asteroid["x"] * 100 + destroyed_asteroid["y"])