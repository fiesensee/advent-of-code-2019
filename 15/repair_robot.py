from intcode_processor import IntcodeProcessor
import queue


class Tile:
    def __init__(self, x, y, parent):
        self.x = x
        self.y = y
        self.parent = parent

    def __str__(self):
        return "{}|{}".format(self.x, self.y)


class RepairRobot:

    def __init__(self):
        self.tiles = []
        intcode_programm = self.read_intcode_programm()
        self.processor = IntcodeProcessor(intcode_programm)
        self.queue = queue.Queue()
        self.current_robot_position = Tile(0, 0, None)

    def read_intcode_programm(self):
        with open("15/input.txt") as f:
            intcode_text = f.readline()
        string_intcodes = intcode_text.split(",")
        intcodes = []
        for intcode in string_intcodes:
            intcodes.append(int(intcode))
        return intcodes

    def move_to_tile(self, tile):
        shared_ancestor = self.get_shared_ancestor(
            self.current_robot_position, tile)
        # move up till it reaches the shared ancestor
        while self.current_robot_position is not shared_ancestor:
            direction_vector = [self.current_robot_position.parent.x - self.current_robot_position.x,
                                self.current_robot_position.parent.y - self.current_robot_position.y]
            direction = self.get_direction_from_vector(direction_vector)
            self.processor.process_intcode_with_input(direction)
            self.current_robot_position = self.current_robot_position.parent
        # go down from the shared ancestor to the target node
        while self.current_robot_position is not tile:
            next_tile = tile
            # determine which one in the parent chain is the next to go to
            while next_tile.parent is not self.current_robot_position:
                next_tile = next_tile.parent
            direction_vector = [next_tile.x - self.current_robot_position.x,
                                next_tile.y - self.current_robot_position.y]
            direction = self.get_direction_from_vector(direction_vector)
            self.processor.process_intcode_with_input(direction)
            # update position of robot except if hit wall
            if self.processor.output is 0:
                break
            self.current_robot_position = next_tile     

    def get_direction_from_vector(self, vector):
        if vector == [0, -1]:
            return 1
        elif vector == [0, 1]:
            return 2
        elif vector == [-1, 0]:
            return 3
        elif vector == [1, 0]:
            return 4
        else:
            print("Error unknown vector {}".format(vector))

    def get_shared_ancestor(self, origin, target):
        origin_parents = [origin]
        target_parents = [target]

        # in case of root node, the shared ancestor is root
        if origin.parent == None:
            return origin

        while not set(origin_parents).intersection(target_parents):
            origin = origin.parent
            origin_parents.append(origin)
            target = target.parent
            target_parents.append(target)

        return list(set(origin_parents).intersection(target_parents))[0]

    def is_tile_visited(self, tile, explored_positions):
        for explored_tile in explored_positions:
            if explored_tile.x == tile.x and explored_tile.y == tile.y:
                return True
        return False

    def explore_tiles(self):
        direction_vectors = [[0, -1], [1, 0], [0, 1], [-1, 0]]
        explored_positions = []
        # move north and south for init?
        self.processor.process_intcode_with_input(3)
        self.processor.process_intcode_with_input(4)
        leak_position = None
        self.queue.put(self.current_robot_position)
        depth = 0
        while not self.queue.empty():
            current_position = self.queue.get()
            print("Looking up {}".format(current_position))
            # go to current_posititon
            self.move_to_tile(current_position)
            # check current position (will be last output of processor)
            result = self.processor.output
            if result == 0:
                # add wall to explored_positons
                explored_positions.append(current_position)
                # no further actions needed
            elif result == 1:
                # add current position to explored_positions (needed why?)
                explored_positions.append(current_position)
                # determine next possible positions and add to queue
                for direction_vector in direction_vectors:
                    next_x = current_position.x + direction_vector[0]
                    next_y = current_position.y + direction_vector[1]
                    next_tile = Tile(next_x, next_y, current_position)
                    if not self.is_tile_visited(next_tile, explored_positions):
                        self.queue.put(next_tile)
                pass
            elif result == 2:
                leak_position = current_position
                break

        tmp_position = leak_position
        while tmp_position.parent is not None:
            tmp_position = tmp_position.parent
            depth += 1
        
        print("Shortest path to {} is {}".format(leak_position, depth))

        self.move_to_tile(leak_position)
        # reinitialize the robot on the leak
        self.current_robot_position = Tile(0,0,None)
        explored_positions = []
        self.queue = queue.Queue()

        self.queue.put(self.current_robot_position)

        while not self.queue.empty():
            current_position = self.queue.get()
            print("Looking up {}".format(current_position))
            # go to current_posititon
            self.move_to_tile(current_position)
            # check current position (will be last output of processor)
            result = self.processor.output
            if result == 0:
                # add wall to explored_positons
                explored_positions.append(current_position)
                # no further actions needed
            elif result == 1 or result == 2:
                # add current position to explored_positions (needed why?)
                explored_positions.append(current_position)
                # determine next possible positions and add to queue
                for direction_vector in direction_vectors:
                    next_x = current_position.x + direction_vector[0]
                    next_y = current_position.y + direction_vector[1]
                    next_tile = Tile(next_x, next_y, current_position)
                    if not self.is_tile_visited(next_tile, explored_positions):
                        self.queue.put(next_tile)
                pass
            # elif result == 2:
                # leak_position = current_position
                # break
        print("gone through all tiles")
        max_depth = 0
        for tile in explored_positions:
            depth = 0
            tmp_tile = tile
            while tmp_tile.parent is not None:
                tmp_tile = tmp_tile.parent
                depth += 1
            if depth >= max_depth:
                max_depth = depth
        
        print("max depth: {}".format(max_depth - 1))

if __name__ == "__main__":
    repair_robot = RepairRobot()
    repair_robot.explore_tiles()
