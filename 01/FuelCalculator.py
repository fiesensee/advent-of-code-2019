import math

def get_masses():
    lines = ""
    with open('./01/masses.txt') as f:
        lines = f.readlines()
    return lines

def calculate_fuel(mass, total):
    fuel = math.floor(mass / 3) - 2
    if (fuel <= 0):
        return total
    else:
        return calculate_fuel(fuel, total + fuel)


if __name__ == "__main__":
    # print(calculate_fuel(100756, 0))
    total_fuel = 0
    masses = get_masses()
    for mass in masses:
        fuel = calculate_fuel(int(mass), 0)
        total_fuel += fuel
    print(total_fuel)