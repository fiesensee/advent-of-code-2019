def intlist(n):
    q = n
    ret = []
    while q != 0:
        q, r = divmod(q, 10) # Divide by 10, see the remainder
        ret.insert(0, r) # The remainder is the first to the right digit
    return ret

def contains_neighbour_double(pin):
    pin_list = intlist(pin)
    for i in range(1, len(pin_list) - 2):
        w = pin_list[i - 1]
        x = pin_list[i]
        y = pin_list[i+1]
        z = pin_list[i+2]
        if x == y:
            if x != w and y != z:
                return True
        if i == len(pin_list) - 3 and y == z and x != z:
            return True
        if i == 1 and w == x and x != y:
            return True
    return False

def is_sorted(pin):
    sorted_pins = intlist(pin)
    sorted_pins.sort()
    if intlist(pin) == sorted_pins:
        return True
    return False

if __name__ == "__main__":
    print(contains_neighbour_double(1111144))
    valid_pins = 0
    pins = []
    for pin in range(125730,579381):
        if contains_neighbour_double(pin) and is_sorted(pin):
            valid_pins += 1
            pins.append(pin)

    print(pins[0])
    print(pins[-1:])
    print(valid_pins)