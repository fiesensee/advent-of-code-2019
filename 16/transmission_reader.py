class TransmissionReader:

    def __init__(self):
        self.current_pattern_count = 0
        self.current_pattern_element_index = 0
        self.pattern_elements = [0,1,0,-1]

    def read_input(self):
        input = []
        with open("16/input.txt") as f:
            line = f.readline().strip()
            for element in line:
                input.append(int(element))
        return input

    def multiply_input(self, input, factor):
        new_input = []
        for _ in range(factor):
            new_input.extend(input)
        return new_input

    def get_offset(self, sub_list):
        offset_list = []
        for element in sub_list:
            offset_list.append(str(element))
        return int("".join(offset_list))


    def get_pattern_element(self, index, step):
        if self.current_pattern_count >= step - 1:
            self.current_pattern_count = 0
            return self.get_next_pattern_element()
        else:
            self.current_pattern_count += 1
            return self.pattern_elements[self.current_pattern_element_index]    
        
    def get_next_pattern_element(self):
        if self.current_pattern_element_index == 3:
            self.current_pattern_element_index = 0
            return self.pattern_elements[self.current_pattern_element_index]
        else:
            self.current_pattern_element_index += 1
            return self.pattern_elements[self.current_pattern_element_index]
    
    def get_pattern_element2(self, index, step):
        if (index - 1 - step) % 2 == 0:
            return 1
        elif (index - 1 - step) % 4 ==0:
            return -1

    def read_transmission(self):
        input = self.read_input()
        input = self.multiply_input(input, 10000)
        offset = self.get_offset(input[:7])
        for phases in range(100):
            new_input = input.copy()
            print("calculating phase {}".format(phases))
            index = len(input) - 1
            sum = 0
            while index >= offset:
                new_input[index] = int(str(input[index] + sum)[-1:])
                sum += input[index]
                index -= 1
            input = new_input

            # new_input = []
            # for i in range(len(input)):
            #     self.current_pattern_count = 0
            #     print("doing step {}".format(i))
            #     sum = 0
            #     for index in range(len(input)):
            #         input_element = input[index]
            #         pattern_element = self.get_pattern_element(index, i)
            #         if pattern_element == 1:
            #             sum += input_element
            #         elif pattern_element == -1:
            #             sum -= input_element
            #         else:
            #             index += i - 1
            #     # print(str(sum)[-1:])
            #     new_input.append(int(str(sum)[-1:]))
            # # print(new_input)
            # input = new_input
        print(input[offset:offset+8])

if __name__ == "__main__":
    transmission_reader = TransmissionReader()
    transmission_reader.read_transmission()
    

                    