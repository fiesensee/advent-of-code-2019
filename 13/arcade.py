from intcode_processor import IntcodeProcessor
# import curses
import time

class Arcade:

    def __init__(self):
        self.tiles = []
        self.score = 0

    def read_intcode_programm(self):
        with open("13/input.txt") as f:
            intcode_text = f.readline()
        string_intcodes = intcode_text.split(",")
        intcodes = []
        for intcode in string_intcodes:
            intcodes.append(int(intcode))
        return intcodes

    def insert_into_tiles(self, new_tile):
        if len(self.tiles) == 0:
            self.tiles.append(new_tile)
        else:
            for tile in self.tiles:
                if tile["x"] == new_tile["x"] and tile["y"] == new_tile["y"]:
                    tile["type"] = new_tile["type"]
                    return
            # if tile not exists yet
            self.tiles.append(new_tile)

    # stdscr = curses.initscr()

    def draw_board(self):
        biggest_x = max([tile["x"] for tile in self.tiles])
        biggest_y = max([tile["y"] for tile in self.tiles])

        board = []

        for _ in range(biggest_y + 1):
            row = []
            for _ in range(biggest_x + 1):
                row.append("")
            board.append(row)

        for tile in self.tiles:
            if tile["type"] == 0:
                board[tile["y"]][tile["x"]] = " "
            elif tile["type"] == 1:
                board[tile["y"]][tile["x"]] = "█"
            elif tile["type"] == 2:
                board[tile["y"]][tile["x"]] = "#"
            elif tile["type"] == 3:
                board[tile["y"]][tile["x"]] = "-"
            elif tile["type"] == 4:
                board[tile["y"]][tile["x"]] = "@"
        
        board_display = ""

        for row in board:
            for point in row:
                board_display += point
            board_display += "\n"

        print(board_display, end="\n")

        # time.sleep(1)

        # stdscr.addstr(0, 0, board_display)
        # stdscr.refresh()

    def update_board(self, outputs):
        i = 0
        while i < len(outputs):
            new_tile = {}
            new_tile["x"] = int(outputs[i])
            new_tile["y"] = int(outputs[i + 1])
            new_tile["type"] = int(outputs[i + 2])
            if new_tile["x"] == -1:
                self.score = new_tile["type"]
            else:
                self.insert_into_tiles(new_tile)
            i += 3

    def count_block_tiles(self):
        block_tiles = []
        for tile in self.tiles:
            if tile["type"] == 2:
                block_tiles.append(tile)
        return len(block_tiles)

    def count_ball_tiles(self):
        block_tiles = []
        for tile in self.tiles:
            if tile["type"] == 4:
                block_tiles.append(tile)
        return len(block_tiles)


    def get_position_of_type(self, tile_type):
        for tile in self.tiles:
            if tile["type"] == tile_type:
                return [tile["x"], tile["y"]]
        return None

    def get_next_input(self, last_ball_position):
        current_ball_position = self.get_position_of_type(4)
        ball_vector = [current_ball_position[0] - last_ball_position[0], current_ball_position[1] - last_ball_position[1]]
        current_cursor_position = self.get_position_of_type(3)
        # check if ball is falling
        if ball_vector[1] > 0:
            # try to catch ball
            height = current_cursor_position[1] - current_ball_position[1]
            prediction = [current_ball_position[0] + (height * ball_vector[0]), current_cursor_position[1]]
            if current_cursor_position[0] < prediction[0]:
                return 1
            elif current_cursor_position[0] > prediction[0]:
                return -1
            else:
                return 0
        else:
            # move to the middle
            if current_cursor_position[0] < current_ball_position[0]:
                return 1
            elif current_cursor_position[0] > current_ball_position[0]:
                return -1
            else:
                return 0

    def run_arcade(self):
        intcode_programm = self.read_intcode_programm()
        intcode_programm[0] = 2
        processor = IntcodeProcessor(intcode_programm)
        input = 0
        processor.process_intcode_with_input(input)
        self.update_board(processor.outputs)
        last_ball_position = self.get_position_of_type(4)
        # print(self.count_ball_tiles())
        # self.draw_board()
        while self.count_block_tiles() != 0:
            processor.process_intcode_with_input(input)
            self.update_board(processor.outputs)
            input = self.get_next_input(last_ball_position)
            last_ball_position = self.get_position_of_type(4)
            # print(self.count_ball_tiles())
            # self.draw_board()
            print(self.count_block_tiles())
        print(self.score)



if __name__ == "__main__":
    arcade = Arcade()
    arcade.run_arcade()
    # draw_board(tiles)
    # block_tiles = []
    # for tile in tiles:
    #     if tile["type"] == 2:
    #         block_tiles.append(tile)
    # print(len(block_tiles))