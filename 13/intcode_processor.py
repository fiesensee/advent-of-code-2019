class IntcodeProcessor:

    def __init__(self, intcode):
        if isinstance(intcode, list):
            self.state = {}
            self.parse_list(intcode)
        else:
            self.state = intcode
        self.pointer = 0
        self.output = 0
        self.outputs = []
        self.is_halted = False
        self.relative_base = 0
    
    def parse_list(self, intcode_list):
        for i in range(len(intcode_list)):
            intcode = intcode_list[i]
            self.write_value(i, intcode)

    def process_intcode_with_input(self, input):
        is_consumed = False
        self.outputs = []
        self.output = 0
        while True:
            intcode = self.read_value(self.pointer)
            op_code = int(str(intcode)[-2:])
            if intcode == 99:
                self.is_halted = True
                break
            elif op_code == 1:
                x, y, z = self.get_n_refrences(str(intcode), 3)

                sum = self.read_value(x) + self.read_value(y)
                self.write_value(z, sum)
                self.pointer += 4
                # print("Opcode: {} X: {} Y: {} Result: {} Write To: {}".format(intcode, x, y, sum, position))
            elif op_code == 2:
                x, y, z = self.get_n_refrences(str(intcode), 3)

                product = self.read_value(x) * self.read_value(y)
                self.write_value(z, product)
                self.pointer += 4
                # print("Opcode: {} X: {} Y: {} Result: {} Write To: {}".format(intcode, x, y, product, position))
            elif op_code == 3:
                if is_consumed:
                    # pointer += 2
                    break
                position = self.get_n_refrences(str(intcode), 1)[0]
                self.write_value(position, input)
                is_consumed = True
                self.pointer += 2
            elif op_code == 4:
                self.output = self.read_value(self.get_n_refrences(str(intcode), 1)[0])
                self.outputs.append(self.output)
                # print("Output: {}".format(self.output))
                self.pointer += 2
            elif op_code == 5:
                x, y = self.get_n_refrences(str(intcode), 2)
                if self.read_value(x) != 0:
                    self.pointer = self.read_value(y)
                else:
                    self.pointer += 3
            elif op_code == 6:
                x, y = self.get_n_refrences(str(intcode), 2)
                if self.read_value(x) == 0:
                    self.pointer = self.read_value(y)
                else:
                    self.pointer += 3
            elif op_code == 7:
                x, y, z = self.get_n_refrences(str(intcode), 3)
                if self.read_value(x) < self.read_value(y):
                    self.write_value(z, 1)
                else:
                    self.write_value(z, 0)
                self.pointer += 4
            elif op_code == 8:
                x, y, z = self.get_n_refrences(str(intcode), 3)
                if self.read_value(x) == self.read_value(y):
                    self.write_value(z, 1)
                else:
                    self.write_value(z, 0)
                self.pointer += 4
            elif op_code == 9:
                new_base = self.read_value(self.get_n_refrences(str(intcode), 1)[0])
                self.relative_base += new_base
                self.pointer += 2
            else:
                self.pointer += 1

    def read_value(self, position):
        if position not in self.state:
            self.state[position] = 0
        return self.state[position]

    def write_value(self, position, value):
        self.state[position] = value

    def get_n_refrences(self, intcode, parameters):
        result = []

        for i in range(parameters):
            if len(intcode) >= i + 3:
                mode = intcode[-i - 3]
                if mode == '0':
                    result.append(self.read_value(self.pointer + i + 1))
                elif mode == '1':
                    result.append(self.pointer + i + 1)
                elif mode == '2':
                    result.append(self.relative_base + self.read_value(self.pointer + i + 1))
                else:
                    print("Error")
                    print(intcode)

            else:
                result.append(self.read_value(self.pointer + i + 1))

        return result

    # def get_2_parameters(self, current_intcode):
    #     x = None
    #     y = None
    #     if len(str(current_intcode)) == 1:
    #         x = self.state[self.state[self.pointer + 1]]
    #         y = self.state[self.state[self.pointer + 2]]
    #     elif len(str(current_intcode)) == 3:
    #         # is 102 in any case
    #         x = self.state[self.pointer + 1]
    #         y = self.state[self.state[self.pointer + 2]]
    #     elif len(str(current_intcode)) == 4:
    #         # can be 1102 or 1002
    #         if str(current_intcode)[1] == "0":
    #             # is 1002
    #             x = self.state[self.state[self.pointer + 1]]
    #             y = self.state[self.pointer + 2]
    #         else:
    #             # is 1102
    #             x = self.state[self.pointer + 1]
    #             y = self.state[self.pointer + 2]

    #     return x, y
