from intcode_processor import IntcodeProcessor

def read_intcode_programm():
    with open("11/input.txt") as f:
        intcode_text = f.readline()
    string_intcodes = intcode_text.split(",")
    intcodes = []
    for intcode in string_intcodes:
        intcodes.append(int(intcode))
    return intcodes

def find_panel(target_panel, panels):
    for panel in panels:
        if panel["x"] == target_panel["x"] and panel["y"] == target_panel["y"]:
            return panel
    return None

def get_movement_vector(rotation, current_direction):
    movement_vector = [0,0]

    # turn left 90 degrees
    if rotation == 0:
        if current_direction == [0,-1]:
            movement_vector = [-1,0]
        elif current_direction == [1,0]:
            movement_vector = [0,-1]
        elif current_direction == [0,1]:
            movement_vector = [1,0]
        elif current_direction == [-1,0]:
            movement_vector = [0,1]
        else:
            print("Error")
    # turn right 90 degrees
    if rotation == 1:
        if current_direction == [0,-1]:
            movement_vector = [1,0]
        elif current_direction == [1,0]:
            movement_vector = [0,1]
        elif current_direction == [0,1]:
            movement_vector = [-1,0]
        elif current_direction == [-1,0]:
            movement_vector = [0,-1]
        else:
            print("Error")
    return movement_vector

def draw_panels(panels):
    biggest_x = max([panel["x"] for panel in panels])
    biggest_y = max([panel["y"] for panel in panels])

    drawing = []

    for _ in range(biggest_y + 1):
        row = []
        for _ in range(biggest_x + 1):
            row.append(".")
        drawing.append(row)
    
    for panel in panels:
        color = panel["color"]
        if color == 0:
            symbol = "."
        else:
            symbol = "#"
        x = panel["x"]
        y = panel["y"]
        drawing[y][x] = symbol

    drawing_text = ""
    for row in drawing:
        for panel in row:
            drawing_text += panel
        drawing_text += "\n"

    print(drawing_text)


if __name__ == "__main__":
    painted_panels = []
    intcode_programm = read_intcode_programm()
    processor = IntcodeProcessor(intcode_programm)
    current_direction = [0,-1]
    current_panel = {"x": 0, "y": 0, "color": 1}
    painted_panels.append(current_panel)
    while not processor.is_halted:
        # determine existing color for program input
        # existing_color = 0
        # if find_panel(current_panel, painted_panels):
        #     existing_color = find_panel(current_panel, painted_panels).color

        # paint the panel
        processor.process_intcode_with_input(current_panel["color"])
        new_color = processor.outputs[0]

        # update the color on the panel or add if it did not exist yet
        current_panel["color"] = new_color

        # get movement vector
        rotation = processor.outputs[1]
        current_direction = get_movement_vector(rotation, current_direction)
        new_panel = {"x": current_panel["x"] + current_direction[0], "y": current_panel["y"] + current_direction[1], "color": 0}
        if find_panel(new_panel, painted_panels):
            new_panel = find_panel(new_panel, painted_panels)
        else:
            painted_panels.append(new_panel)

        # print("New Color: {} Rotation: {} Current Direciton: {}".format(new_color, rotation, current_direction))
        # print(current_panel)
        # print(new_panel)
        current_panel = new_panel


    draw_panels(painted_panels)
    print(len(painted_panels))

